import { id } from "../../src/id";
import { AsDocument } from "../../src/as-document";
import { timestamps } from "../../src/timestamps";
import { FromSchema } from "json-schema-to-ts"

export const schema = {
  type: "object",
  properties: {
    name: { type: "string" }
  },
  required: []
} as const

export type Dummy = FromSchema<typeof schema>

export type DummyDocument = AsDocument<Dummy>

export const dummyDocument = {
  ...schema,
  properties: {
    ...schema.properties,
    ...id.properties,
    ...timestamps.properties,
  },
  required: [
    ...(schema.required || []),
    ...id.required,
    ...timestamps.required
  ]
}