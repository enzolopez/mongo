import { createDataMapper } from "../../src/data-mapper"
import { Dummy, DummyDocument } from "../model/dummy"

export const COLLECTION = 'test'

const dataMapper = createDataMapper<Dummy>(COLLECTION);

// here create wrapper for each function if is necessary
export const create = dataMapper.create;

export const findOneById = (
  id: string
): Promise<null | DummyDocument> => {
  return dataMapper.findOne({_id: id});
}