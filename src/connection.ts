import { Collection, Db, MongoClient as Client } from 'mongodb';
import { ConnectionStatus } from './connection-status';

export type MongoDb = Db;
export type MongoClient = Client;
export type MongoCollection<T> = Collection<T>;

let _client: MongoClient | null = null;
let _db: Db | null = null;

let unresolved = [] as any[];

let status = ConnectionStatus.disconnected;

const MONGO_URL: any = process.env.MONGO_URL;

const createClient = async (): Promise<{ db: Db; client: MongoClient }> => {
  try {
    const client = await Client.connect(MONGO_URL, {
      reconnectInterval: 1000,
      reconnectTries: 60,
      useNewUrlParser: true,
    });

    _db = client.db();

    _client = client;
    status = ConnectionStatus.connected;

    _db.on('close', () => {
      status = ConnectionStatus.disconnected;
    });

    setImmediate(() => {
      for (const promise of unresolved) {
        promise.resolve({ client, db: _db });
      }
      unresolved = [];
    });

    return { client, db: _db };
  } catch (err) {
    for (const promise of unresolved) {
      promise.reject(err);
    }
    unresolved = [];
    throw err;
  }
};

export const connection = async (): Promise<{ readonly db: Db; readonly client: MongoClient }> => {
  if (status === 1 && _client?.isConnected() && _db) {
    return { client: _client, db: _db };
  }

  if (status === 2) {
    return new Promise((resolve, reject) => {
      unresolved.push({ resolve, reject });
    });
  }

  status = ConnectionStatus.connecting;

  return createClient();
};

const start = async () => {
  const { db } = await connection();
  return db;
};

const stop = async () => {
  if (_client?.isConnected()) {
    await _client.close();
  }
};

export const createInstance = (): { stop: () => Promise<void>; start: () => Promise<Db> } => {
  return {
    start,
    stop,
  };
};
