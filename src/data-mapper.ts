import { FilterQuery, InsertOneWriteOpResult, InsertWriteOpResult, SortValues } from 'mongodb';
import { connection } from './connection';
import { AsDocument } from './as-document';

import { v4 as genId } from 'uuid';

export const createDataMapper = <ItemType>(
  collection: string,
): {
  partialUpdate(_id: string, partialProps: Partial<ItemType>): Promise<AsDocument<ItemType> | null>;
  createMany(items: ItemType[]): Promise<AsDocument<ItemType>[]>;
  find(query: FilterQuery<AsDocument<ItemType>>, sort?: Record<symbol, SortValues>): Promise<AsDocument<ItemType>[]>;
  findOne(query: FilterQuery<AsDocument<ItemType>>): Promise<AsDocument<ItemType> | null>;
  create(item: ItemType): Promise<AsDocument<ItemType> | null>;
  update(_id: string, props: ItemType): Promise<AsDocument<ItemType> | null>;
  del(id: string): Promise<boolean>;
} => {
  type DocType = AsDocument<ItemType>;

  return {
    async findOne(query: FilterQuery<DocType>): Promise<null | DocType> {
      const { db } = await connection();

      return db.collection<DocType>(collection).findOne(query);
    },

    async create(item: ItemType): Promise<null | DocType> {
      const { db } = await connection();

      const now = new Date();
      const ids = { id: genId() };
      const tms = { created: now, updated: now };
      const doc = { ...item, ...ids, ...tms };

      const result = (await db
        .collection<DocType>(collection)
        .insertOne(doc as any)) as InsertOneWriteOpResult<DocType>;

      return result.ops.length > 0 ? result.ops[0] : null;
    },

    async createMany(items: ItemType[]): Promise<DocType[]> {
      const { db } = await connection();

      const now = new Date();
      const tms = { created: now, updated: now };

      const docs = items.map((item) => {
        const ids = { _id: genId() };
        return {
          ...item,
          ...ids,
          ...tms,
        };
      });

      const result = (await db.collection<DocType>(collection).insertMany(docs as any)) as InsertWriteOpResult<DocType>;

      return result.ops;
    },

    async update(id: string, props: ItemType): Promise<null | DocType> {
      const { db } = await connection();

      const now = new Date();

      const result = await db
        .collection<DocType>(collection)
        .findOneAndReplace({ _id: id } as FilterQuery<DocType>, { ...props, updated: now }, { returnOriginal: false });

      return result.value || null;
    },

    async partialUpdate(_id: string, partialProps: Partial<ItemType>): Promise<null | DocType> {
      const { db } = await connection();

      const now = new Date();

      const result = await db
        .collection<DocType>(collection)
        .findOneAndUpdate(
          { _id } as FilterQuery<DocType>,
          { $set: { ...partialProps, updated: now } as Partial<DocType> },
          { returnOriginal: false },
        );

      return result.value || null;
    },

    async find(query: FilterQuery<DocType>, sort?: Record<symbol, SortValues>): Promise<DocType[]> {
      const { db } = await connection();

      if (sort) {
        return db
          .collection<DocType>(collection)
          .find(query as FilterQuery<DocType>)
          .sort(sort as Record<string, SortValues>)
          .toArray();
      }

      return db
        .collection<DocType>(collection)
        .find(query as FilterQuery<DocType>)
        .toArray();
    },

    async del(id: string): Promise<boolean> {
      const { db } = await connection();

      const result = await db.collection<DocType>(collection).deleteOne({ _id: id } as FilterQuery<DocType>);

      return result.deletedCount === 1;
    },
  };
};
