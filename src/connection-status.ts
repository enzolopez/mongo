export enum ConnectionStatus {
  'connected' = 0,
  'disconnected' = 1,
  'connecting' = 2,
}
