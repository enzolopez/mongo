import { FromSchema } from 'json-schema-to-ts';

export const timestamps = {
  type: 'object',
  properties: {
    created: {
      type: 'string',
      format: 'date-time',
    },
    updated: {
      type: 'string',
      format: 'date-time',
    },
  },
  required: ['created', 'updated'],
} as const;

export type Timestamps = FromSchema<typeof timestamps>;
