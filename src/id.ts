import { FromSchema } from 'json-schema-to-ts';

export const id = {
  type: 'object',
  properties: {
    _id: {
      type: 'string',
    },
  },
  required: ['_id'],
} as const;

export type Id = FromSchema<typeof id>;
