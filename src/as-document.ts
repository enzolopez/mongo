import { Id } from './id';
import { Timestamps } from './timestamps';

export type AsDocument<Item> = Item & Id & Timestamps;
